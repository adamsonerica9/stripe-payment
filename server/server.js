const express=require("express")
const cors=require("cors")
const dotenv=require("dotenv")
const server=express()
dotenv.config()
const stripe=require("stripe")(process.env.SECRET_KEY)
server.use(cors())
server.use(express.json())
server.use(express.urlencoded({extended:true}))
server.post("/payment-intent", async (req, res, next)=>{
    try{
        const {amount}=req.body
        if(!amount || isNaN(amount)){
            return res.status(400).json({status:false, error:"Invalid Amount"})
        }
        const paymentIntent=await stripe.paymentIntents.create({
            amount: amount,//convert into cents
            currency:"inr",
            description: 'Just testing',
        })
        return res.status(200).json({status:true, paymentIntent})
    }catch(err){
        console.log(err)
        return res.status(500).json({status:false, message:"Internal server error"})
    }
})
server.listen(4000, ()=>{
    console.log("server is running at 4000")
})