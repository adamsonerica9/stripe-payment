import {CardNumberElement, CardExpiryElement, CardCvcElement} from "@stripe/react-stripe-js"
import { useStripe, useElements} from '@stripe/react-stripe-js'
const Checkout=()=>{
    const stripe = useStripe();
    const elements = useElements();
    const handleCheckout=async (e)=>{
        e.preventDefault()
        // make API call
        const response=await fetch(`http://localhost:4000/payment-intent`, {
            method:"POST",
            body:JSON.stringify({amount:200}),
            headers:{
                'Content-Type':'application/json'
            }
        })
        if(response.status===200){
            const data=await response.json()
            console.log(data.paymentIntent.client_secret)
            const confirmPayment=await stripe.confirmCardPayment(data.paymentIntent.client_secret, {
                payment_method:{
                    card:elements.getElement(CardNumberElement)
                }
            })
            if(confirmPayment.paymentIntent.status==="succeeded"){
                console.log("payment confirmed")
            }
        }
    }
    return <form onSubmit={handleCheckout}>
        <CardNumberElement/>
        <CardCvcElement/>
        <CardExpiryElement/>
        <button>Submit</button>
    </form>
}
export default Checkout