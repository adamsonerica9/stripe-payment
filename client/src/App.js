import './App.css';
import CheckoutPage from "./CheckoutPage"
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
const stripePromise = loadStripe(`${process.env.REACT_APP_PUBLISHABLE_KEY}`);
function App() {
  return (
    <div className="App">
      <Elements stripe={stripePromise}>
        <CheckoutPage/>
      </Elements>
    </div>
  );
}
export default App;